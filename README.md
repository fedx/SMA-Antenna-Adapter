# SMA adapter for Antenna

[Hackaday IO writeup](https://hackaday.io/project/187297-radio-antenna-for-sdr)

Remixed from [SMA connector cover](https://www.printables.com/model/175205-sma-connector-cover)

---

When I set out on this project, it started with a broken boom box and a desire to have a portable telescopic antenna. Therefore, I pulled out that antenna and started wondering how exacly to adapt [this](https://www.printables.com/model/175205-sma-connector-cover) to an SMA connector.

I had learned monopole antenna don't need a ground as the laptop and SDR itself act as a ground, therefore I needed to find a way to connect the long mettal bit to the tiny inner connector of the SDR. I therefore grabbed this SMA connector cover and moddeled an addapter based on it which you can see in this repo.
